import urllib.request
import requests
import os
from bs4 import BeautifulSoup
from pybloom_live import ScalableBloomFilter


def main():
    def crawl(depth):
        search = True
        try:
            data = urllib.request.urlopen(str(urls[-1]))
            soup = BeautifulSoup(data)
        except:
            search = False
        while search:
            for link in soup.findAll('a', href=True):
                try:
                    child = link.get('href')
                    temp_url = clean_child(str(urls[-1]), child)
                    if temp_url is not None and temp_url not in sites:
                        sites.add(temp_url)
                        # print(sites.count)
                        # print(temp_url)
                        if 'pdf' in temp_url:
                            print('FOUND PDF')
                            print(sites.count)
                            temp_url = temp_url.replace('epdf', 'pdf')
                            print(temp_url)
                            download_pdf(temp_url, sites.count)
                            search = False
                            break
                        # if depth < max_depth:
                        #     urls.append(temp_url)
                        #     if 'pdf' in temp_url:
                        #         print('FOUND PDF')
                        #         search = False
                        #         break
                        #     crawl(depth+1)
                        else:
                            pass
                except:
                    pass
            depth -= 1
            try:
                del urls[-1]
            except:
                pass
            if depth is 0:
                return str(sites.count)
            else:
                pass

    depth = 1
    max_depth = 1
    sites = ScalableBloomFilter(mode=ScalableBloomFilter.SMALL_SET_GROWTH)
    text_file = '/Users/Carter/Desktop/urls.txt'
    file_names = [line.rstrip('\n') for line in open(text_file)]
    for file in file_names:
        if 'None' not in file:
            urls = [file]
            try:
                print('RESULTS: ' + crawl(depth))
            except:
                print('No crawl on: {}'.format(file))
    os.system('rm -Rf {}'.format(text_file))



def clean_child(url, child):
    if child in ['mailto', '#', '/'] or 'mailto' in url:
        return None
    elif 'http' in child:
        return child
    else:
        return url + child



def download_pdf(url, count):
    location = '/Users/Carter/Desktop/crawlout/{}.pdf'.format(count)
    pdf = requests.get(url, stream=True)
    with open(location, 'wb') as final_pdf:
        for block in pdf.iter_content(512):
            if not block:
                break
            final_pdf.write(block)



if __name__ == '__main__':
    main()
